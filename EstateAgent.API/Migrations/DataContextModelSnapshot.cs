﻿// <auto-generated />
using System;
using EstateAgent.API.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EstateAgent.API.Migrations
{
    [DbContext(typeof(DataContext))]
    partial class DataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024");

            modelBuilder.Entity("EstateAgent.API.Models.Bookmark", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("HouseId");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("HouseId");

                    b.HasIndex("UserId");

                    b.ToTable("Bookmarks");
                });

            modelBuilder.Entity("EstateAgent.API.Models.House", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Bedrooms");

                    b.Property<string>("City");

                    b.Property<string>("County");

                    b.Property<int>("PAON");

                    b.Property<string>("Postcode");

                    b.Property<int>("Price");

                    b.Property<string>("Street");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Houses");
                });

            modelBuilder.Entity("EstateAgent.API.Models.Message", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content");

                    b.Property<DateTime>("DateSent");

                    b.Property<int?>("RecipientId");

                    b.Property<int?>("SenderId");

                    b.HasKey("Id");

                    b.HasIndex("RecipientId");

                    b.HasIndex("SenderId");

                    b.ToTable("Messages");
                });

            modelBuilder.Entity("EstateAgent.API.Models.Photo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateAdded");

                    b.Property<int>("HouseId");

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.HasIndex("HouseId");

                    b.ToTable("Photos");
                });

            modelBuilder.Entity("EstateAgent.API.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created");

                    b.Property<string>("Email");

                    b.Property<DateTime>("LastActive");

                    b.Property<byte[]>("PasswordHash");

                    b.Property<byte[]>("PasswordSalt");

                    b.Property<string>("PhotoUrl");

                    b.Property<string>("Username");

                    b.Property<string>("VerificationCode");

                    b.Property<bool>("Verified");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("EstateAgent.API.Models.Bookmark", b =>
                {
                    b.HasOne("EstateAgent.API.Models.House", "House")
                        .WithMany()
                        .HasForeignKey("HouseId");

                    b.HasOne("EstateAgent.API.Models.User", "User")
                        .WithMany("Bookmarks")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EstateAgent.API.Models.House", b =>
                {
                    b.HasOne("EstateAgent.API.Models.User")
                        .WithMany("Houses")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("EstateAgent.API.Models.Message", b =>
                {
                    b.HasOne("EstateAgent.API.Models.User", "Recipient")
                        .WithMany()
                        .HasForeignKey("RecipientId");

                    b.HasOne("EstateAgent.API.Models.User", "Sender")
                        .WithMany()
                        .HasForeignKey("SenderId");
                });

            modelBuilder.Entity("EstateAgent.API.Models.Photo", b =>
                {
                    b.HasOne("EstateAgent.API.Models.House", "House")
                        .WithMany("Photos")
                        .HasForeignKey("HouseId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}

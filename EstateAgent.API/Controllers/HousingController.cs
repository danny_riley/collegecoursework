using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using EstateAgent.API.Data;
using EstateAgent.API.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EstateAgent.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class HousingController : ControllerBase
    {
        private readonly IHousingRepository _repo;
        private readonly IMapper _mapper;
        public HousingController(IHousingRepository repo, IMapper mapper)
        {
            this._mapper = mapper;
            this._repo = repo;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetHouses()
        {
            var houses = await _repo.GetHouses(); // Uses the repository to get the users from the database
            var housesToReturn = _mapper.Map<IEnumerable<HouseForListDto>>(houses); // Maps the houses' details to a differnt object with less detail
            return Ok(housesToReturn); // Returns the users to the user from the API
        }
        
        [AllowAnonymous]
        [HttpGet("users")]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _repo.GetUsers(); // Uses the repository to get the users from the database
            var usersToReturn = _mapper.Map<IEnumerable<UsersForList>>(users); // Maps the houses' details to a differnt object with less detail
            return Ok(usersToReturn); // Returns the users to the user from the API
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> getHouse(int id)
        {
            var house = await _repo.GetHouse(id); // Gets the house from the database
            var houseToReturn = _mapper.Map<HouseForDetailedDto>(house); // Maps the house's details to a differnt object
            return Ok(houseToReturn); // Returns the house to the caller
        }

        [HttpGet("user/{userId}")]
        public async Task<IActionResult> GetUsersHouses(int userId) 
        {
            var user = await _repo.GetUser(userId); // Gets the user from the user Id
            if(user == null){ return Ok(null); } // Checks to see if the user exists in the database
            var houses = await _repo.GetUserHouses(user); // Gets the houses from the user
            var housesToReturn = _mapper.Map<IEnumerable<HouseForListDto>>(houses); // Maps the houses to the house list object
            return Ok(housesToReturn);
        }

        [AllowAnonymous]
        [HttpPost("bookmark")]
        public async Task<IActionResult> AddBookmark(BookmarkDto bookmarkDto)
        {
            await _repo.AddBookmark(bookmarkDto.HouseId, bookmarkDto.UserId);
            return Ok(true);
        }

        [AllowAnonymous]
        [HttpGet("bookmark/{userId}")]
        public async Task<IActionResult> GetBookmarks(int userId)
        {
            var bookmarks = await _repo.GetBookmarks(userId);
            return Ok(bookmarks);
        }
    }
}
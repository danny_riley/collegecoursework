using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using EstateAgent.API.Data;
using EstateAgent.API.Dtos;
using EstateAgent.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace EstateAgent.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _repo;
        private readonly IConfiguration _config;
        private readonly IHousingRepository _houseRepo;
        public AuthController(IAuthRepository repo, IConfiguration config, IHousingRepository houseRepo)
        {
            this._config = config;
            this._repo = repo;
            this._houseRepo = houseRepo;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegisterDto userForRegisterDto)
        {
            userForRegisterDto.Username = userForRegisterDto.Username.ToLower().Trim();

            if (await _repo.UserExists(userForRegisterDto.Username))
                return BadRequest("Username already exists");

            var userToCreate = new User
            {
                Username = userForRegisterDto.Username
            };

            var createdUser = await _repo.Register(userToCreate, userForRegisterDto.Password);

            return StatusCode(201); // Created at route status code place holder
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDto userForLoginDto)
        {
            var userFromRepo = await _repo.Login(userForLoginDto.Username
            .ToLower().Trim(), userForLoginDto.Password);

            if (userFromRepo == null) // Checks to see if the username and password is correct or not
                return Unauthorized();

            var claims = new[]{
                new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                new Claim(ClaimTypes.Name, userFromRepo.Username)
            }; // Generates the contents of the token

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config
            .GetSection("AppSettings:Token").Value)); // Gets the encryption key

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            // Creates the credentials which the server signs the token with

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            }; // Creates the descriptor for the token payload

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor); // Creates the token for the user

            return Ok(new 
            {
                token = tokenHandler.WriteToken(token) // Generates a response and returns the token
            });                                        // to the user
        }
    }
}
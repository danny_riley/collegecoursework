using System.Collections.Generic;
using EstateAgent.API.Models;

namespace EstateAgent.API.Dtos
{
    public class HouseForDetailedDto
    {
        public int Id { get; set; }
        public int PAON { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public int Price { get; set; }
        public int Bedrooms { get; set; }
        public ICollection<PhotosForDetailedDto> Photos { get; set; }
    }
}
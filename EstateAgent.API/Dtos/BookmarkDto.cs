namespace EstateAgent.API.Dtos
{
    public class BookmarkDto
    {
        public int HouseId { get; set; }
        public int UserId { get; set; }
    }
}
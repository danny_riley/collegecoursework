namespace EstateAgent.API.Models
{
    public class Bookmark
    {
        public int Id { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public House House { get; set; }
    }
}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

baseUrl = 'http://localhost:5000/api/auth/';
jwtHelper = new JwtHelperService();
decodedToken: any;

constructor(private http: HttpClient) { }

login(model: any) {
  return this.http.post(this.baseUrl + 'login', model) // Sends a request to the API and gets
    .pipe(                                             // A JSON web token
      map((response: any) => {
        const user = response;
        if (user) {
          localStorage.setItem('token', user.token); // Stores the token in the browser storage
          this.decodedToken = this.jwtHelper.decodeToken(user.token); // Decodes the token and stores it
        }
      })
    );
}

register(model: any) {
  return this.http.post(this.baseUrl + 'register', model);
}

loggedIn() {
  const token = localStorage.getItem('token');
  return !this.jwtHelper.isTokenExpired(token);
}

}

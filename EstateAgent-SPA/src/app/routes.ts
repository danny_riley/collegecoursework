import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HouseListComponent } from './house-list/house-list.component';
import { MessagesComponent } from './messages/messages.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { AuthGuard } from './_guards/auth.guard';
import { UserListComponent } from './user-list/user-list.component';

export const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {
        path: '', // A path which all guarded routes have to go through
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard], // Activates the AuthGuard component
        children: // Only allows users who are logged in to access these routes
        [
            {path: 'houses', component: HouseListComponent},
            {path: 'messages', component: MessagesComponent},
            {path: 'bookmarks', component: BookmarksComponent},
            {path: 'users', component: UserListComponent}
        ]
    },
    {path: '**', redirectTo: '', pathMatch: 'full'} // Goes to the home component if no route is given
    // Or the wrong route is given
];

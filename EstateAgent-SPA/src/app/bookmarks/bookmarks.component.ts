import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {

  data: any;
  bookmarks: any;

  constructor(private http: HttpClient, public authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.getBookmarks();
  }

  getBookmarks() {
    this.http.get('http://localhost:5000/api/housing/bookmark/' + this.authService.decodedToken.nameid).subscribe(response => {
      this.bookmarks = response;
      console.log(this.bookmarks);
      console.log(this.bookmarks[0]['house']['photos'][0]);
    }, error => {
      console.log(error);
    });
  }

  addBookmark(houseId: string) {
    this.data = {
      'userid': this.authService.decodedToken.nameid,
      'houseid': houseId
    };

    this.http.post('http://localhost:5000/api/housing/bookmark', this.data).subscribe(response => {
      this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(() =>
      this.router.navigate(['bookmarks']));

    });
  }



}

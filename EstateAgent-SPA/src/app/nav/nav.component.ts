import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  model: any = {};

  constructor(public authService: AuthService, private alertify: AlertifyService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.model).subscribe(next => {
      this.alertify.success('Logged in successfully'); // Sends notification to the user
    },
    error => {
      this.alertify.error(error); // Sends error message to the user
    }, () => {
      this.router.navigate(['/houses']); // Navigates to the houses list when the user logs in
    });
  }

  loggedIn() {
    return this.authService.loggedIn(); // Checks the token for validity by making a call to the service
  }

  logout() {
    localStorage.removeItem('token'); // Removes the token from local storage
    this.alertify.message('Logged out'); // Displayes the logged out message to the user
    this.router.navigate(['/home']); // Navigates to the home page when the user logs out
  }

}
